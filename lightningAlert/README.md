# Lightning Alert Notifications
This application reads lightning data as a stream from standard input (one lightning strike per line as a JSON object, and matches that data against a source of assets (also in JSON format) to produce an alert.
Please see the README in the parent folder to see accurate format. Since **FlashType : 9** is a Heartbeat input, we ignore these messages and does not issue a notification for such data.

## Prerequisites ##
- Java 1.8
- maven 3.8

## To build ##
- Check out this source to your machine
- cd into lightningAlert (under parent dir lightning-alert) and run following commands
  - **mvn package**   (generates lightningAlert-1.0-SNAPSHOT-jar-with-dependencies jar in your target folder)
  - **java -jar target/lightningAlert-1.0-SNAPSHOT-jar-with-dependencies.jar < src/main/resources/lightning.json**    (you could use a json file as standard input as I have done here using the provided sample file lightning.json. Alternatively you could type in the json format when prompted)
   
## Time Complexity ##
Time complexity for determining if a strike has occurred for a particular asset is 
Big O (number of assets registered in that particular quadKey location)
## Improvements ##
This application could be made more scalable if input could be processed parallely.
In production settings, it might mean introducing loadbalancers to manage the load.
