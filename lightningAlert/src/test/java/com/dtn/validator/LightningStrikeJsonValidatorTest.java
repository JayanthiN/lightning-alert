package com.dtn.validator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LightningStrikeJsonValidatorTest {
    String VALID_JSON;
    String INVALID_JSON_MORE_FIELDS;
    String INVALID_JSON_LESS_FIELDS;
    String INVALID_JSON;

    @Before
    public void setUp() throws Exception {
        VALID_JSON = "{\"flashType\": 1,\"strikeTime\": 1386285909025,\"latitude\": 33.5524951,\"longitude\": -94.5822016,\"peakAmps\": 15815," +
                "\"reserved\": \"000\",\"icHeight\": 8940,\"receivedTime\": 1386285919187,\"numberOfSensors\": 17,\"multiplicity\": 1}";
        INVALID_JSON_MORE_FIELDS = "{\"flashType\": 1,\"strikeTime\": 1386285909025,\"latitude\": 33.5524951,\"longitude\": -94.5822016,\"peakAmps\": 15815," +
                "\"reserved\": \"000\",\"icHeight\": 8940,\"receivedTime\": 1386285919187,\"numberOfSensors\": 17,\"multiplicity\": 1,\"randomField\":\"withRandomValue\"}";
        INVALID_JSON_LESS_FIELDS = "{\"flashType\": 1,\"strikeTime\": 1386285909025}";
        INVALID_JSON = "Hello World!";

    }

    @Test
    public void isValid_LightningStrikeJSON_ValidFormat() {

        assertTrue(LightningStrikeJsonValidator.isValid(VALID_JSON));
    }

    @Test
    public void isValid_LightningStrikeJSON_InvalidFormat() {

        assertFalse(LightningStrikeJsonValidator.isValid(INVALID_JSON));
    }

    @Test
    public void isValid_LightningStrikeJSON_InvalidFormat_MoreFIelds() {

        assertFalse(LightningStrikeJsonValidator.isValid(INVALID_JSON_MORE_FIELDS));
    }

    @Test
    public void isValid_LightningStrikeJSON_InvalidFormat_LessFIelds() {

        assertFalse(LightningStrikeJsonValidator.isValid(INVALID_JSON_LESS_FIELDS));
    }

}