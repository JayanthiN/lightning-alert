package com.dtn.service;

import com.dtn.compute.TileSystem;
import com.dtn.event.lightning.LightningStrike;
import com.google.gson.JsonSyntaxException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LightningServiceTest {
    LightningService lightningService;

    @Before
    public void setUp() throws Exception {
        lightningService = new LightningService();
    }

    @Test
    public void mapToLightningStrike_validJSON() {
        String VALID_JSON = "{\"flashType\": 1,\"strikeTime\": 1386285909025,\"latitude\": 33.5524951,\"longitude\": -94.5822016,\"peakAmps\": 15815," +
                "\"reserved\": \"000\",\"icHeight\": 8940,\"receivedTime\": 1386285919187,\"numberOfSensors\": 17,\"multiplicity\": 1}";
        LightningStrike lightningStrike_EXPECTED = new LightningStrike(1, 1386285909025L, 33.5524951, -94.5822016, 15815, "000", 8940, 1386285919187L, 17, 1);
        //set quadKeyfield
        lightningStrike_EXPECTED.setQuadKey(TileSystem.LatLongToQuadKey(lightningStrike_EXPECTED.getLatitude(), lightningStrike_EXPECTED.getLongitude()));
        LightningStrike lightningStrike_ACTUAL = lightningService.mapToLightningStrike(VALID_JSON);
        assertEquals(lightningStrike_EXPECTED.toString(), lightningStrike_ACTUAL.toString());
    }

    @Test(expected = JsonSyntaxException.class)
    public void mapToLightningStrike_InalidJSON() {
        String INVALID_JSON = "Hello";
        LightningStrike lightningStrike_ACTUAL = lightningService.mapToLightningStrike(INVALID_JSON);
    }
}