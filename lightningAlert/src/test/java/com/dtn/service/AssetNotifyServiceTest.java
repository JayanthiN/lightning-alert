package com.dtn.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AssetNotifyServiceTest {
    AssetNotifyService assetNotifyService;

    @Before
    public void setUp() throws Exception {
        assetNotifyService = new AssetNotifyService();
    }

    @Test
    public void generateAssetNotifications_WhenQuadKeyPresent() {
        //quadkey present 023112320003
        String EXPECTED_OUTPUT = "lightning alert for 132:Raynor Avenue";
        String actualOutput = assetNotifyService.generateAssetNotifications("023112320003");
        assertEquals(actualOutput, EXPECTED_OUTPUT);
    }

    @Test
    public void generateAssetNotifications_WhenQuadKeyAbsent() {
        //quadkey absent 023119999999
        String EMPTY = "";
        String actualOutput = assetNotifyService.generateAssetNotifications("023119999999");
        System.out.println(actualOutput);
        assertEquals(actualOutput, EMPTY);
    }
}