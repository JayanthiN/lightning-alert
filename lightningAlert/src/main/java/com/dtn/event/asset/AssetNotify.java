package com.dtn.event.asset;

import com.dtn.entity.Asset;

/**
 * Asset Notify Event
 */
public class AssetNotify {
    private Asset asset;
    private boolean isNotified;

    public AssetNotify(Asset asset, boolean isNotified) {
        this.asset = asset;
        this.isNotified = isNotified;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public boolean isNotified() {
        return isNotified;
    }

    public void setNotified(boolean notified) {
        isNotified = notified;
    }

    @Override
    public String toString() {
        return "lightning alert for " + asset.getAssetOwner() + ":" + asset.getAssetName();
    }
}
