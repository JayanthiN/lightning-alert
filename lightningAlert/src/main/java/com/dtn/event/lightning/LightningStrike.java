package com.dtn.event.lightning;


/**
 * Mapping POJO class for JSON Object lightningStrike
 * Ex: {"flashType": 1,"strikeTime": 1386285909025, "latitude": 33.5524951, "longitude": -94.5822016, "peakAmps": 15815,
 * "reserved": "000","icHeight": 8940,"receivedTime": 1386285919187,"numberOfSensors": 17,"multiplicity": 1}
 */
public class LightningStrike {


    private Integer flashType;

    private Long strikeTime;

    private Double latitude;

    private Double longitude;

    private Integer peakAmps;

    private String reserved;

    private Integer icHeight;

    private Long receivedTime;

    private Integer numberOfSensors;

    private Integer multiplicity;


    private String quadKey;

    public LightningStrike() {

    }

    public LightningStrike(int flashType, Long strikeTime, double latitude, double longitude, int peakAmps, String reserved, int icHeight, Long receivedTime, int numberOfSensors, int mulitplicity) {
        this.flashType = flashType;
        this.strikeTime = strikeTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.peakAmps = peakAmps;
        this.reserved = reserved;
        this.icHeight = icHeight;
        this.receivedTime = receivedTime;
        this.numberOfSensors = numberOfSensors;
        this.multiplicity = mulitplicity;
    }

    public Integer getFlashType() {
        return flashType;
    }

    public void setFlashType(Integer flashType) {
        this.flashType = flashType;
    }

    public Long getStrikeTime() {
        return strikeTime;
    }

    public void setStrikeTime(Long strikeTime) {
        this.strikeTime = strikeTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getPeakAmps() {
        return peakAmps;
    }

    public void setPeakAmps(Integer peakAmps) {
        this.peakAmps = peakAmps;
    }

    public String getReserved() { return reserved; }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public Integer getIcHeight() {
        return icHeight;
    }

    public void setIcHeight(Integer icHeight) {
        this.icHeight = icHeight;
    }

    public Long getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(Long receivedTime) {
        this.receivedTime = receivedTime;
    }

    public Integer getNumberOfSensors() {
        return numberOfSensors;
    }

    public void setNumberOfSensors(Integer numberOfSensors) {
        this.numberOfSensors = numberOfSensors;
    }

    public Integer getMultiplicity() {
        return multiplicity;
    }

    public void setMultiplicity(Integer multiplicity) {
        this.multiplicity = multiplicity;
    }

    public String getQuadKey() {
        return quadKey;
    }

    public void setQuadKey(String quadKey) {
        this.quadKey = quadKey;
    }

    @Override
    public String toString() {
        return "lightningStrike{" +
                "flashType=" + flashType +
                ", strikeTime=" + strikeTime +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", peakAmps=" + peakAmps +
                ", reserved='" + reserved + '\'' +
                ", icHeight=" + icHeight +
                ", receivedTime=" + receivedTime +
                ", numberOfSensors=" + numberOfSensors +
                ", multiplicity=" + multiplicity +
                ", quadKey=" + quadKey +
                '}';
    }
}
