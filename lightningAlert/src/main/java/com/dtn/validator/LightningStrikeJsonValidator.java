package com.dtn.validator;

import com.dtn.adapter.LongAdapter;
import com.dtn.event.lightning.LightningStrike;
import com.google.gson.*;
import java.util.Map;

public final class LightningStrikeJsonValidator {
    private static Gson gson;
    //total number of fields in LightningStrike class , but one less to account  for the additional computed field for quadKey
    private static final int EXPECTED_NO_OF_FIELDS = LightningStrike.class.getDeclaredFields().length - 1;
    static{
        gson = new GsonBuilder().registerTypeAdapter(Number.class, new LongAdapter()).create();
    }


    public static boolean isValid(String jsonString) {
        boolean invalidData = false;
        JsonObject jsonObj = null;

        try {
             jsonObj = JsonParser.parseString(jsonString).getAsJsonObject();
            if (jsonObj.entrySet().size() > EXPECTED_NO_OF_FIELDS) {
                return false;
            }
        }catch(JsonSyntaxException|IllegalStateException e){
            return false;
        }

       int count=0;
        for(Map.Entry<String, JsonElement> jsonEntry:jsonObj.entrySet()) {

             invalidData = inValidFieldInLightningStrike(jsonEntry);

           if(invalidData) {
               break;
           }
           count++;
        }
        if(count!=EXPECTED_NO_OF_FIELDS)
            invalidData = true;
        return !invalidData;
    }
    /*
      Check if the given json input confirms to all the fields/types expected in LightningStrike event
    */
    private static boolean inValidFieldInLightningStrike(Map.Entry<String, JsonElement> jsonEntry) {

        boolean invalidData = false;
        switch (jsonEntry.getKey()) {
            case "flashType":
                if(!(Integer.valueOf(jsonEntry.getValue().toString()) instanceof Integer)) {
                    System.out.println(jsonEntry.toString());
                    invalidData = true;
                }
                break;
            case "strikeTime":
                if(!(Long.valueOf(jsonEntry.getValue().toString()) instanceof Long)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "latitude":
                if(!(Double.valueOf(jsonEntry.getValue().toString()) instanceof Double)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "longitude":
                if(!(Double.valueOf(jsonEntry.getValue().toString()) instanceof Double)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "peakAmps":
                if(!(Integer.valueOf(jsonEntry.getValue().toString()) instanceof Integer)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "reserved":
                if(!(String.valueOf(jsonEntry.getValue().toString()) instanceof String)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "icHeight":
                if(!(Integer.valueOf(jsonEntry.getValue().toString()) instanceof Integer)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "receivedTime":
                if(!(Long.valueOf(jsonEntry.getValue().toString()) instanceof Long)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "numberOfSensors":
                if(!(Integer.valueOf(jsonEntry.getValue().toString()) instanceof Integer)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            case "multiplicity":
                if(!(Integer.valueOf(jsonEntry.getValue().toString()) instanceof Integer)) {
                    invalidData = true;
                    System.out.println(jsonEntry.toString());
                }
                break;
            default:
                System.out.println(jsonEntry.getValue()+"Unknown Value");
                invalidData = true;

                break;
        }
       return invalidData;
    }
}
