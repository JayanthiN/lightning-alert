package com.dtn.service;


import com.dtn.adapter.LongAdapter;
import com.dtn.compute.TileSystem;
import com.dtn.event.lightning.LightningStrike;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * Service class for Lightning Strikes
 * reads JSON input to map to LightningStrike event Object
 */
public class LightningService {
    static Gson gson;

    static {
        gson = new GsonBuilder().registerTypeAdapter(Number.class, new LongAdapter()).create();
    }


    public LightningStrike mapToLightningStrike(String stJSON) {

        LightningStrike lStrike = null;

        lStrike = gson.fromJson(stJSON, LightningStrike.class);

        lStrike.setQuadKey(TileSystem.LatLongToQuadKey(lStrike.getLatitude(), lStrike.getLongitude()));

        return lStrike;
    }


}
