package com.dtn.service;

import com.dtn.entity.Asset;
import com.dtn.event.asset.AssetNotify;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service class for Asset Noitifcations
 * Reads assets JSON file to create a map structure with quadKey as key
 */
public class AssetNotifyService {
    static Gson gson;
    static Map<String, List<AssetNotify>> assetNotiftMap;
    static InputStream inputStream;
    static String filename;

    static {
        gson = new Gson();
        filename = "assets.json";
        inputStream = AssetNotifyService.class.getClassLoader().getResourceAsStream(filename);
        assetNotiftMap = quadKeyMappedAssets(inputStream);
    }

   /*
     Create a Map structure with quadKey as Key for faster Access
     The value field of each key has list of AssetNotify objects that has the same quadKey
     A boolean flag in assetNotify is initialized to false,
     indicating no asset is notified yet of any lightning
    */

    private static Map<String, List<AssetNotify>> quadKeyMappedAssets(InputStream inputStream) {
        Asset[] assets = null;
        Map<String, List<AssetNotify>> alertMap = new HashMap<String, List<AssetNotify>>();
        Path path = Paths.get(filename);
        try {
            Reader reader = new InputStreamReader(inputStream);
            assets = gson.fromJson(reader, Asset[].class);
            inputStream.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Asset asset : assets) {
            if (!alertMap.containsKey(asset.getQuadKey()))
                alertMap.put(asset.getQuadKey(), new ArrayList<AssetNotify>());
            alertMap.get(asset.getQuadKey()).add(new AssetNotify(asset, false));
        }
        return alertMap;
    }

    /*
       if a quadKey is present in the AssetNotify map, fetches the list of AssetsNotify event objects that has this key,
       appends all those events in a stringbuilder and returns as string
    */
    public String generateAssetNotifications(String quadKey) {
        StringBuilder sb = new StringBuilder();
        if (assetNotiftMap.containsKey(quadKey)) {
            List<AssetNotify> assetNotifications = assetNotiftMap.get(quadKey);
            for (AssetNotify assetNotify : assetNotifications) {
                if (!assetNotify.isNotified()) {
                    sb.append(assetNotify);
                    assetNotify.setNotified(true);
                }
            }

        }
        return sb.toString();
    }
}
