package com.dtn.entity;

/**
 * Mapping POJO class for Asset JsonObject
 * Ex:{"assetName":"Mayer Park","quadKey":"023112133002","assetOwner":"02115"}
 */
public class Asset {
    private String assetName;

    private String quadKey;

    private String assetOwner;

    public Asset() {
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getQuadKey() {
        return quadKey;
    }

    public void setQuadKey(String quadKey) {
        this.quadKey = quadKey;
    }

    public String getAssetOwner() {
        return assetOwner;
    }

    public void setAssetOwner(String assetOwner) {
        this.assetOwner = assetOwner;
    }

    @Override
    public String toString() {
        return "Asset{" +
                "assetName='" + assetName + '\'' +
                ", quadKey='" + quadKey + '\'' +
                ", assetOwner='" + assetOwner + '\'' +
                '}';
    }
}
