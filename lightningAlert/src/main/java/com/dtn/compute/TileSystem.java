package com.dtn.compute;

/**
 * Courtsey: Adapted from article https://docs.microsoft.com/en-us/bingmaps/articles/bing-maps-tile-system?redirectedfrom=MSDN
 * (for Java)
 */
public class TileSystem {

    private static final double EarthRadius = 6378137;
    private static final double MinLatitude = -85.05112878;
    private static final double MaxLatitude = 85.05112878;
    private static final double MinLongitude = -180;
    private static final double MaxLongitude = 180;
    private static final int LevelOfDetail = 12;


    /* Clips a number to the specified minimum and maximum values.
      <param name="n">The number to clip.</param>
      <param name="minValue">Minimum allowable value.</param>
      <param name="maxValue">Maximum allowable value.</param>
       <returns>The clipped value.</returns>
    */
    private static double Clip(double n, double minValue, double maxValue) {
        return Math.min(Math.max(n, minValue), maxValue);
    }

    /*
     Determines the map width and height (in pixels) at a specified level of detail.
     <param name="levelOfDetail">Level of detail, from 1 (lowest detail)
     to 23 (highest detail).</param>
     <returns>The map width and height in pixels.</returns>
    */
    public static int MapSize(int levelOfDetail) {
        return 256 << levelOfDetail;
    }

    /*
     Converts tile XY coordinates into a QuadKey at a level of detail 12.
     <param name="tileX">Tile X coordinate.</param>
     <param name="tileY">Tile Y coordinate.</param>
     <param name="levelOfDetail">Level of detail, from 1 (lowest detail) to 23 (highest detail).</param>
     <returns>A string containing the QuadKey.</returns>
    */
    public static String TileXYToQuadKey(int tileX, int tileY) {
        StringBuilder quadKey = new StringBuilder();
        //its given as part of the problem to assume LevelOfDetail to be 12
        for (int i = LevelOfDetail; i > 0; i--) {
            char digit = '0';
            int mask = 1 << (i - 1);
            if ((tileX & mask) != 0) {
                digit++;
            }
            if ((tileY & mask) != 0) {
                digit++;
                digit++;
            }
            quadKey.append(digit);
        }
        return quadKey.toString();
    }


    /*
      Converts a point from latitude/longitude WGS-84 coordinates (in degrees) into pixel XY coordinates
      at a specified level of detail, and then to TileXY coordinates and then to quadKey
      <param name="latitude">Latitude of the point, in degrees.</param>
      <param name="longitude">Longitude of the point, in degrees.</param>
      <param name="levelOfDetail">Level of detail, from 1 (lowest detail) to 23 (highest detail).</param>
      <param name="pixelX">Output parameter receiving the X coordinate in pixels.</param>
      <param name="pixelY">Output parameter receiving the Y coordinate in pixels.</param>
     */
    public static String LatLongToQuadKey(double latitude, double longitude) {
        latitude = Clip(latitude, MinLatitude, MaxLatitude);
        longitude = Clip(longitude, MinLongitude, MaxLongitude);
        double x = (longitude + 180) / 360;
        double sinLatitude = Math.sin(latitude * Math.PI / 180);
        double y = 0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);
        //its given as part of the problem to assume LevelOfDetail to be 12
        int mapSize = MapSize(LevelOfDetail);
        int pixelX = (int) Clip(x * mapSize + 0.5, 0, mapSize - 1);
        int pixelY = (int) Clip(y * mapSize + 0.5, 0, mapSize - 1);
        // Convert pixel XY coordinates into tile XY coordinates of the tile
        int tileX = pixelX / 256;
        int tileY = pixelY / 256;
        //computeQuadKey from tileX,tileY
        return TileXYToQuadKey(tileX, tileY);
    }

}
