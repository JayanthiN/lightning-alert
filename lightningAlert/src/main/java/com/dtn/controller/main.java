package com.dtn.controller;

import com.dtn.event.lightning.LightningStrike;
import com.dtn.service.AssetNotifyService;
import com.dtn.service.LightningService;
import com.dtn.validator.LightningStrikeJsonValidator;

import java.util.Scanner;

/**
 * main entry point to lightningAlert application , accepts JSON string from standard input,
 * converts JSON to LightningStrike object,
 * generates assetNotifications for all the assets that belong to the location of lightning Strike
 * The generated assetNotification messages are printed out to console
 */
public class main {
    private static final int FLASHTYPE_HEARTBEAT = 9;
    LightningService lightningService = new LightningService();
    AssetNotifyService assetNotifyService = new AssetNotifyService();

    /*
      Accepts input from standard input, validates it for a properly formatted lightningstrike json object
      and proceeds to process the input
     */
    public static void main(String[] args) {
        Scanner scanConsole = new Scanner(System.in);
        main main = new main();
        System.out.println("Please enter valid JSON string for lightning strike (see ReadMe for correct format)");

        while (scanConsole.hasNextLine()) {
            String stJSON = scanConsole.nextLine();
            //System.out.println("Received :"+stJSON);
            if (LightningStrikeJsonValidator.isValid(stJSON)) {
                main.processInput(stJSON);
            } else {
                System.out.println("Invalid/Incomplete data. Please verify input JSON data");
            }

        }
    }

    /*
      Process the json input using service methods
      First maps given json string to the lightning strike event object
      generate asset notifications and prints them if the flashtype is not HEARTBEAT
     */
    public void processInput(String stJSON) {
        LightningStrike lightningStrike = lightningService.mapToLightningStrike(stJSON);
        if (lightningStrike.getFlashType() != FLASHTYPE_HEARTBEAT) {
            String assetNotification = assetNotifyService.generateAssetNotifications(lightningStrike.getQuadKey());
            if (assetNotification.length() > 0)
                System.out.println(assetNotification);
        }
    }


}
